#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color

echo -e "${GREEN}Runing the app.${NC}"
pelican content -s pelicanconf.py --autoreload  --listen --port 8000 --bind 0.0.0.0