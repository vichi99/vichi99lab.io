# Requirements stage
FROM python:3.10-slim as requirements-stage

WORKDIR /usr/src

RUN pip install poetry

COPY ./pyproject.toml ./poetry.lock* /usr/src/

RUN poetry export --without-hashes -f requirements.txt --output requirements.txt

# app
FROM python:3.10-slim

COPY app /usr/src/app

COPY --from=requirements-stage /usr/src/requirements.txt /usr/src/app/requirements.txt

WORKDIR /usr/src/app

RUN apt update

RUN pip install --no-cache-dir --upgrade -r /usr/src/app/requirements.txt

CMD ["./startup.sh"]
